# vim:set et sts=4 sw=4 fileencoding=utf-8:
#
# ibus-hangul - The Hangul Engine For IBus
#
# Copyright (c) 2009-2011 Choe Hwanjin <choe.hwanjin@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
from gi.repository import Gtk
from gi.repository import GdkPixbuf
import gettext

import config

_ = lambda a : gettext.dgettext(config.gettext_package, a)
N_ = lambda a: a

IBUS_HANGUL_LICENCE = _('License: GPL-2+\n'
                'This program is free software; you can redistribute it and/or \n'
                'modify it under the terms of the GNU General Public License as \n'
                'published by the Free Software Foundation, either version 2 of \n'
                'the License, or (at your option) any later version.\n\n'
                'See the GNU General Public License for more details.\n\n'
                'You should have received a copy of the GNU General Public \n'
                'License along with this program.\n'
                'If not, see <http://www.gnu.org/licenses/>.'
                )

class AboutDialog ():
    def __init__ (self, transient_for=None):
        aboutdialog = Gtk.AboutDialog()
        aboutdialog.set_transient_for(transient_for)
        aboutdialog.set_program_name("IBus-hangul")
        aboutdialog.set_version(config.version)
        aboutdialog.set_copyright(_("Copyright (c) 2007-2009. Huang Peng\n"
                            "Copyright (c) 2005-2006,2009-2014. Choe Hwanjin \n"
                            "All rights reserved"))
        aboutdialog.set_comments(_("ibus-hangul is a Korean input method engine for IBus"))
        aboutdialog.set_license(IBUS_HANGUL_LICENCE)
        aboutdialog.set_wrap_license(True)
        aboutdialog.set_website("https://github.com/choehwanjin/")
        aboutdialog.set_website_label("IBus-hangul Website")
        aboutdialog.set_authors([_("Choe Hwan-Jin")])
        aboutdialog.set_documenters([_("Choe Hwan-Jin")])
        aboutdialog.set_artists([_("Choe Hwan-Jin")])
        aboutdialog.set_translator_credits(_("Choe Hwan-Jin"))
        logo_file = os.path.join(config.pkgdatadir, 'icons', 'ibus-hangul.png')
        aboutdialog.set_logo(GdkPixbuf.Pixbuf.new_from_file(logo_file))
        #aboutdialog.set_logo_icon_name("icon name")

        aboutdialog.run()
        aboutdialog.destroy()
