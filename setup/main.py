# vim:set et sts=4 sw=4:
#
# ibus-hangul - The Hangul Engine For IBus
#
# Copyright (c) 2009-2011 Choe Hwanjin <choe.hwanjin@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import sys
import os
import gi
from gi.repository import Gio
from gi.repository import GLib
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('IBus', '1.0')
from gi.repository import IBus
import locale
import gettext
import config
from keycapturedialog import KeyCaptureDialog

from aboutdialog import AboutDialog
from layoutdialog import LayoutDialog

_ = lambda a : gettext.dgettext(config.gettext_package, a)
N_ = lambda a: a


def get_hangul_keyboard_list():
    from ctypes import CDLL, c_int, c_char_p
    libhangul = CDLL('libhangul.so.1')
    libhangul.hangul_ic_get_n_keyboards.argtypes = []
    libhangul.hangul_ic_get_n_keyboards.restype = c_int
    libhangul.hangul_ic_get_keyboard_id.argtypes = [c_int]
    libhangul.hangul_ic_get_keyboard_id.restype = c_char_p
    libhangul.hangul_ic_get_keyboard_name.argtypes = [c_int]
    libhangul.hangul_ic_get_keyboard_name.restype = c_char_p

    n = libhangul.hangul_ic_get_n_keyboards()
    list = []
    for i in range(n):
        id = libhangul.hangul_ic_get_keyboard_id(i).decode('UTF-8')
        name = libhangul.hangul_ic_get_keyboard_name(i).decode('UTF-8')
        list.append((id, name))
    return list


class Setup ():
    def __init__ (self, bus):
        self.__bus = bus
        self.__settings = Gio.Settings(schema="org.freedesktop.ibus.engine.hangul")
        self.__settings.connect("changed", self.on_value_changed)
        self.__settings_panel = Gio.Settings(schema="org.freedesktop.ibus.panel")
        self.__settings_panel.connect("changed", self.on_value_changed)

        ui_file = os.path.join(os.path.dirname(__file__), "setup.ui")
        self.__builder = Gtk.Builder()
        self.__builder.set_translation_domain(config.gettext_package)
        self.__builder.add_from_file(ui_file)


        self._mask = 0
        # Hangul tab
        self.__hangul_keyboard_select = self.__builder.get_object("HangulSelectButton")
        self.__hangul_keyboard_select.connect("clicked", \
                self.on_keyboard_layout_select_clicked, "hangul-keyboard")

        self.__hangul_keyboard_list = get_hangul_keyboard_list()
        
        self.__hangul_keyboard = self.__builder.get_object("HangulSelectEntry")
        self._hangul_key = self.__read("hangul-keyboard").get_string()
        for (key, name) in self.__hangul_keyboard_list:
            if key == self._hangul_key:
                self.__hangul_keyboard.set_text(name)
                self.__hangul_keyboard.set_tooltip_text(key)
                break

        self.__start_in_hangul_mode = self.__builder.get_object("StartInHangulMode")
        initial_input_mode = self.__read("initial-input-mode").get_string()
        self.__start_in_hangul_mode.set_active(initial_input_mode == "hangul")

        self.__word_commit = self.__builder.get_object("WordCommit")
        word_commit = self.__read("word-commit").get_boolean()
        self.__word_commit.set_active(word_commit)

        self.__auto_reorder = self.__builder.get_object("AutoReorder")
        auto_reorder = self.__read("auto-reorder").get_boolean()
        self.__auto_reorder.set_active(auto_reorder)

        button = self.__builder.get_object("HangulKeyListAddButton")
        button.connect("clicked", self.on_key_add, "hangul-keys")

        button = self.__builder.get_object("HangulKeyListRemoveButton")
        button.connect("clicked", self.on_key_remove, "hangul-keys")

        model = Gtk.ListStore(str)

        keylist_str = self.__read("switch-keys").get_string()
        self.__hangul_key_list_str = keylist_str.split(',')
        for i in self.__hangul_key_list_str:
            model.append([i])

        self.__hangul_key_list = self.__builder.get_object("HangulKeyList")
        self.__hangul_key_list.set_model(model)
        column = Gtk.TreeViewColumn()
        column.set_title("key")
        renderer = Gtk.CellRendererText()
        column.pack_start(renderer, True)
        column.add_attribute(renderer, "text", 0)
        self.__hangul_key_list.append_column(column)

        # hanja tab
        button = self.__builder.get_object("HanjaKeyListAddButton")
        button.connect("clicked", self.on_key_add, "hanja-keys")

        button = self.__builder.get_object("HanjaKeyListRemoveButton")
        button.connect("clicked", self.on_key_remove, "hanja-keys")

        model = Gtk.ListStore(str)

        keylist_str = self.__read("hanja-keys").get_string()
        self.__hanja_key_list_str = keylist_str.split(',')
        for i in self.__hanja_key_list_str:
            model.append([i])

        self.__hanja_key_list = self.__builder.get_object("HanjaKeyList")
        self.__hanja_key_list.set_model(model)
        column = Gtk.TreeViewColumn()
        column.set_title("key")
        renderer = Gtk.CellRendererText()
        column.pack_start(renderer, True)
        column.add_attribute(renderer, "text", 0)
        self.__hanja_key_list.append_column(column)

        # advanced tab
        #notebook = self.__builder.get_object("SetupNotebook")
        #notebook.remove_page(2)

        # notebook 104 keyboard
        self.__enable_104_keys = self.__builder.get_object("Keyboard104Keys")
        self.__enable_104_keys.set_active(self.__read("enable-104-keyboard").get_boolean())
        self.__enable_104_keys.connect('toggled', self.apply_sensitive_toggle, "enable-104-keyboard")

        # extended layout
        self.__enable_layout_extended = self.__builder.get_object("ExtensionFeatureEnable")
        self.__enable_layout_extended.set_active(self.__read("enable-layout-extended").get_boolean())
        self.__enable_layout_extended.connect('toggled', self.apply_sensitive_toggle, "enable-layout-extended")

        # galmadeuli feature
        self.__enable_method_galmadeuli = self.__builder.get_object("GalmadeuliFeatureEnable")
        self.__enable_method_galmadeuli.set_active(self.__read("enable-method-galmadeuli").get_boolean())
        self.__enable_method_galmadeuli.connect('toggled', self.apply_sensitive_toggle, "enable-method-galmadeuli")

        # hidden flag
        self.hidden_flags_sensitive ()

        # use-event-forwarding
        self.__use_event_forwarding = self.__builder.get_object("UseEventForwardingEnable")
        self.__use_event_forwarding.set_active(self.__read("use-event-forwarding").get_boolean())
        self.__use_event_forwarding.connect('toggled', self.apply_sensitive_toggle, "use-event-forwarding")

        # lookup table orientation
        self._lookup_table_orientation = self.__settings_panel.get_value("lookup-table-orientation")
        self.__lookup_vertical = self.__builder.get_object('LookupTableOrientationVertical')
        self.__lookup_horizontal = self.__builder.get_object('LookupTableOrientationHorizontal')

        if self._lookup_table_orientation.get_int32() == 1:
            self.__lookup_vertical.set_active(True)
        else:
            self.__lookup_horizontal.set_active(True)
        
        self.__lookup_vertical.connect('toggled', self.on_radio_button_toggled)
        self.__lookup_horizontal.connect('toggled', self.on_radio_button_toggled)



        # initial language
        current = self.__read("initial-input-mode")
        self.__initial_language = self.__builder.get_object("InitialLanguage")

        model = self.__initial_language.get_model()
        #str, str, int   #_("English"), 'english', 0
        self._init_lang_list_model = {'name': 0,'id': 1,'index': 2}
        if model == None:
            model = Gtk.ListStore(str, str, int)
            for (name, key, index) in [(N_("Latin"), 'latin', 0), (N_("Hangul"), 'hangul', 1)]:
                model.append([_(name), key, index])

            self.__initial_language.set_model(model)
            renderer = Gtk.CellRendererText()
            self.__initial_language.pack_start(renderer, True)
            self.__initial_language.add_attribute(renderer, "text", self._init_lang_list_model['name'])

        initial_language = current.get_string()

        for row in model:
            if row[self._init_lang_list_model['id']] == initial_language:
                self.__initial_language.set_active(row[self._init_lang_list_model['index']])
                break

        self.__initial_language.connect('changed', self.apply_sensitive_toggle, "initial-input-mode")

        # flush interval time
        self.__flush_time_select = self.__builder.get_object("IntervalTime")
        flush_time = self.__read("preedit-flush-time").get_uint32()

        if flush_time < 100 or flush_time > 5000:
            flush_time = 1000
        self.__flush_time_select.set_range(10, 5000)
        self.__flush_time_select.set_increments(10, 100)
        self.__flush_time_select.set_value(flush_time)
        self.__flush_time_select.set_tooltip_text(_("Primary button: 10 steps\n"
            "Middle button: 100 steps\n"
            "Minimum: 10\n"
            "Maximum: 5000"))

        self.__flush_time_select.connect("value-changed", self.apply_sensitive_toggle, 
            "preedit-flush-time")
        
        # enable auto flush
        enable_flush_time = self.__read("enable-preedit-flush").get_boolean()
        self.__enable_flush_time = self.__builder.get_object("IntervalTimeEnable")
        self.__enable_flush_time.set_active(enable_flush_time)

        self.__enable_flush_time.connect('toggled', self.flush_use_sensitive, 
            "enable-preedit-flush")
        if enable_flush_time == False:
            self.__builder.get_object('IntervalTimeLabel').set_sensitive(False)
            self.__builder.get_object('IntervalTimeSpinBox').set_sensitive(False)

        # setup dialog
        self.__window = self.__builder.get_object("SetupDialog")
        icon_file = os.path.join(config.pkgdatadir, "icons", "ibus-hangul.svg")
        self.__window.set_icon_from_file(icon_file)
        self.__window.connect("destroy", Gtk.main_quit)
        self.__window.show()
        self.__window.connect("destroy", Gtk.main_quit)


        button = self.__builder.get_object("button_about")
        button.connect("clicked", self.on_about, None)

        button = self.__builder.get_object("button_apply")
        button.connect("clicked", self.on_apply, None)

        button = self.__builder.get_object("button_cancel")
        button.connect("clicked", self.on_cancel, None)
        #button.grap_focus()

        button = self.__builder.get_object("button_ok")
        button.connect("clicked", self.on_ok, None)

    def run(self):
        Gtk.main()

    def apply_sensitive_toggle(self, widget, mask_key):
        found = False

        for entry in config.VALUE_MASK['engine/hangul']:
            if mask_key == entry:
                self._mask |= config.VALUE_MASK['engine/hangul'][mask_key]
                found = True
                break

        if found == True:
            self.__builder.get_object('button_apply').set_sensitive(True)

    def flush_use_sensitive(self, widget, mask_key):
        enable_flush_time = widget.get_active()
        self.__builder.get_object('IntervalTimeLabel').set_sensitive(enable_flush_time)
        self.__builder.get_object('IntervalTimeSpinBox').set_sensitive(enable_flush_time)
        self.apply_sensitive_toggle (None, mask_key)

    def hidden_flags_sensitive(self, widget = None, mask_key = None):
        # hidden flag
        hidden_flags = self.__read("hidden-flag").get_byte()

        if hidden_flags & config.VALUE_MASK['hidden_flag']['HANGUL_KEYBOARD_FLAG_EXTENDED']:
            self.__builder.get_object('AdvancedExtensionFeature').set_sensitive(True)
        else:
            self.__builder.get_object('AdvancedExtensionFeature').set_sensitive(False)

        if hidden_flags & config.VALUE_MASK['hidden_flag']['HANGUL_KEYBOARD_FLAG_GALMADEULI']:
            self.__builder.get_object('AdvancedGalmadeuliFeature').set_sensitive(True)
        else:
            self.__builder.get_object('AdvancedGalmadeuliFeature').set_sensitive(False)
        
        self.apply_sensitive_toggle(None, "hidden-flag")

    def apply(self):
        # hangul keyboard
        if self._mask & config.VALUE_MASK['engine/hangul']["hangul-keyboard"]:
            current = self.__hangul_keyboard.get_text()
            for (key, name) in self.__hangul_keyboard_list:
                if _(name) == _(current):
                    self._hangul_key = key
                    break
            self.__write("hangul-keyboard", GLib.Variant.new_string(self._hangul_key))

        start_in_hangul_mode = self.__start_in_hangul_mode.get_active()
        if start_in_hangul_mode:
            self.__write("initial-input-mode", GLib.Variant.new_string("hangul"))
        else:
            self.__write("initial-input-mode", GLib.Variant.new_string("latin"))

        word_commit = self.__word_commit.get_active()
        self.__write("word-commit", GLib.Variant.new_boolean(word_commit))

        auto_reorder = self.__auto_reorder.get_active()
        self.__write("auto-reorder", GLib.Variant.new_boolean(auto_reorder))

        model = self.__hangul_key_list.get_model()
        str = ""
        iter = model.get_iter_first()
        while iter:
            if len(str) > 0:
                str += ","
                str += model.get_value(iter, 0)
            else:
                str += model.get_value(iter, 0)
            iter = model.iter_next(iter)
        self.__write("switch-keys", GLib.Variant.new_string(str))

        model = self.__hanja_key_list.get_model()
        str = ""
        iter = model.get_iter_first()
        while iter:
            if len(str) > 0:
                str += ","
                str += model.get_value(iter, 0)
            else:
                str += model.get_value(iter, 0)
            iter = model.iter_next(iter)
        self.__write("hanja-keys", GLib.Variant.new_string(str))

        # advanced
        if self._mask & config.VALUE_MASK['engine/hangul']["enable-104-keyboard"]:
            enable_104_keys = self.__enable_104_keys.get_active()
            self.__write("enable-104-keyboard", GLib.Variant.new_boolean(enable_104_keys))

        if self._mask & config.VALUE_MASK['engine/hangul']["enable-layout-extended"]:
            enable_layout_extended = self.__enable_layout_extended.get_active()
            self.__write("enable-layout-extended", GLib.Variant.new_boolean(enable_layout_extended))

        if self._mask & config.VALUE_MASK['engine/hangul']["enable-method-galmadeuli"]:
            enable_method_galmadeuli = self.__enable_method_galmadeuli.get_active()
            self.__write("enable-method-galmadeuli", GLib.Variant.new_boolean(enable_method_galmadeuli))

        if self._mask & config.VALUE_MASK['engine/hangul']["use-event-forwarding"]:
            use_event_forwarding = self.__use_event_forwarding.get_active()
            self.__write("use-event-forwarding", GLib.Variant.new_boolean(use_event_forwarding))

        if self._mask & config.VALUE_MASK['engine/hangul']["lookup-table-orientation"]:
            self.__settings_panel.set_value("lookup-table-orientation", self._lookup_table_orientation)

        if self._mask & config.VALUE_MASK['engine/hangul']["initial-input-mode"]:
            model = self.__initial_language.get_model()
            row = self.__initial_language.get_active()
            self.__write("initial-input-mode", GLib.Variant.new_string(model[row][self._init_lang_list_model['id']]))

        if self._mask & config.VALUE_MASK['engine/hangul']["preedit-flush-time"]:
            flush_time = self.__flush_time_select.get_value_as_int()
            self.__write("preedit-flush-time", GLib.Variant.new_uint32(flush_time))

        if self._mask & config.VALUE_MASK['engine/hangul']["enable-preedit-flush"]:
            enable_flush_time = self.__enable_flush_time.get_active()
            self.__write("enable-preedit-flush", GLib.Variant.new_boolean(enable_flush_time))

    def on_about(self, widget, data):
        dialog = AboutDialog(transient_for=self.__window)

    def on_apply(self, widget, data):
        self.apply()

    def on_cancel(self, widget, data):
        self.__window.destroy()
        Gtk.main_quit()

    def on_ok(self, widget, data):
        self.apply()
        self.__window.destroy()
        Gtk.main_quit()

    def on_keyboard_layout_select_clicked(self, widget, mask_key):
        if mask_key == "hangul-keyboard" :
            current_layout_id = self._hangul_key
            layout_list = self.__hangul_keyboard_list
        elif mask_key == 'keyboard_english':
            current_layout_id = self.english_key
            layout_list = self.__english_keyboard_list
        else:
            return

        dialog = LayoutDialog(transient_for=self.__window, \
                    current_layout_id=current_layout_id, \
                    layout_list=layout_list)

        response = dialog.run()
        if response != Gtk.ResponseType.APPLY:
            dialog.destroy()
            return

        self.apply_sensitive_toggle (None, mask_key)
        (key, name) = dialog.get_selected_layout()
        dialog.destroy()

        if key != None:
            if mask_key == "hangul-keyboard" :
                self.__hangul_keyboard.set_text(name)
                self.__hangul_keyboard.set_tooltip_text(key)
            elif mask_key == 'keyboard_english':
                self.__english_keyboard_entry.set_text(name)
                self.__english_keyboard_entry.set_tooltip_text(key)

    def on_key_add(self, widget, mask_key = None):
        if mask_key == "hangul-keys":
            title = _("Select Hangul toggle key")
            desc = _("Press any key which you want to use as hangul toggle key. " \
                "The key you pressed is displayed below.\n" \
                "If you want to use it, click \"Ok\" or click \"Cancel\"")
            key_list = self.__hangul_key_list
        elif mask_key == "hanja-keys":
            title = _("Select Hanja key")
            desc = _("Press any key which you want to use as hanja key. "
                "The key you pressed is displayed below.\n"
                "If you want to use it, click \"Ok\" or click \"Cancel\"")
            key_list = self.__hanja_key_list
        else:
            return

        dialog = KeyCaptureDialog(title, self.__window)
        dialog.set_markup(desc)
        res = dialog.run()
        if res == Gtk.ResponseType.OK:
            key_str = dialog.get_key_string()
            if len(key_str) > 0:
                model = key_list.get_model()
                iter = model.get_iter_first()
                while iter:
                    str = model.get_value(iter, 0)
                    if str == key_str:
                        model.remove(iter)
                        break
                    iter = model.iter_next(iter)

                model.append([key_str])
        dialog.destroy()

        self.apply_sensitive_toggle (None, mask_key)

    def on_key_remove(self, widget, mask_key = None):
        if mask_key == "hangul-keys":
            selection = self.__hangul_key_list.get_selection()
        elif mask_key == "hanja-keys":
            selection = self.__hanja_key_list.get_selection()
        else:
            return

        (model, iter) = selection.get_selected()
        if model and iter:
            model.remove(iter)
        
        self.apply_sensitive_toggle (None, mask_key)

    def on_radio_button_toggled(self, widget, data = None):
        if not widget.get_active():
            return

        if widget == self.__lookup_vertical:
            self._lookup_table_orientation = GLib.Variant.new_int32(1)
        elif widget == self.__lookup_horizontal:
            self._lookup_table_orientation = GLib.Variant.new_int32(0)
        
        self.apply_sensitive_toggle (None, "lookup-table-orientation")

    def on_value_changed(self, settings, key):
        value = settings.get_value(key)
        if key.endswith('keyboard'):
            if key.startswith('hangul'):
                for (key, name) in self.__hangul_keyboard_list:
                    if key == value.get_string():
                        self.__hangul_keyboard.set_text(name)
                        self.__hangul_keyboard.set_tooltip_text(key)
                        break
        elif key == "switch-keys":
            self.__hangul_key_list_str = value.get_string().split(',')
        elif key == "hanja-keys":
            self.__hanja_key_list_str = value.get_string().split(',')
        elif key.startswith('enable'):
            if key.endswith('keyboard'):
                self.__enable_104_keys.set_active(value.get_boolean())
            elif key.endswith('extended'):
                self.__enable_layout_extended.set_active(value.get_boolean())
            elif key.endswith('galmadeuli'):
                self.__enable_method_galmadeuli.set_active(value.get_boolean())
        elif key == "use-event-forwarding":
            self.__use_event_forwarding.set_active(value.get_boolean())
        elif key == "lookup-table-orientation":
            self._lookup_table_orientation = self.__settings_panel.get_value(key)
            if self._lookup_table_orientation.get_int32() == 1:
                self.__lookup_vertical.set_active(True)
            else:
                self.__lookup_horizontal.set_active(True)
        elif key.startswith('hidden'):
            if key.endswith('flag'):
                # hidden flag
                self.hidden_flags_sensitive ()
        elif key == "initial-input-mode":
            model = self.__initial_language.get_model()
            for row in model:
                if row[self._init_lang_list_model['id']] == value:
                    self.__initial_language.set_active(row[self._init_lang_list_model['index']])
                    break

        self.__builder.get_object('button_apply').set_sensitive(False)

    def __read(self, key):
        return self.__settings.get_value(key)

    def __write(self, key, v):
        self.__settings.set_value(key, v)

if __name__ == "__main__":
    locale.bindtextdomain(config.gettext_package, config.localedir)

    GLib.set_prgname("ibus-setup-hangul")
    GLib.set_application_name(_("IBusHangul Setup"))

    bus = IBus.Bus()
    if bus.is_connected():
        Setup(bus).run()
    else:
        message = _("IBus daemon is not running.\nHangul engine settings cannot be saved.")
        dialog = Gtk.MessageDialog(type = Gtk.MessageType.ERROR,
                                   buttons = Gtk.ButtonsType.CLOSE,
                                   message_format = message)
        dialog.run()
        sys.exit(1)
